//
//  main.m
//  SMPairing
//
//  Created by Julien Fantin on 13/02/13.
//  Copyright (c) 2013 Stylemarks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SMAppDelegate class]));
    }
}
